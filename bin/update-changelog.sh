#!/usr/bin/env bash

# to update changelog

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}
mkdir -p ${LOG_DIR}

echo "projectId: $PROJECT"
echo "version:   $VERSION"
echo "log dir:   $LOG_DIR"

echo "Update changelog : $VERSION ($LOG_DIR/update-changelog.log)"
rm -rf target/gitlab-cache
mvn -N -Pupdate-changelog -Dgitlab.quiet=false --log-file ${LOG_DIR}/update-changelog.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_DIR}/update-changelog.log
   exit 1
fi
git commit -m "Update changelog after release of milestone $VERSION" CHANGELOG.md
git push