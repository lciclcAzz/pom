#!/usr/bin/env bash

# to drop a stage

STAGE=$1
PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}
mkdir -p ${LOG_DIR}

echo "projectId: $PROJECT"
echo "stage:     $STAGE"
echo "log dir:   $LOG_DIR"

echo "Get staging id..."
STAGE_ID=$(mvn nexus-staging:rc-list -N | grep ${STAGE} | cut -d' ' -f2)

echo "Dropping stage: $STAGE_ID ($LOG_DIR/stage-drop.log)"
mvn nexus-staging:drop -N -DstagingRepositoryId=${STAGE_ID} --log-file ${LOG_DIR}/stage-drop.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_DIR}/stage-drop.log
   exit 1
fi