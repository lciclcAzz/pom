#!/usr/bin/env bash

# to update staging changelog

STAGE=$1
PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)

MASTER=$(grep -e ".masterBranchName" pom.xml | grep -v "{" | cut -d'>' -f2 | cut -d'<' -f1)
if [ "$MASTER" == "" ]; then
  MASTER="master"
fi
DEVELOP=$(grep -e ".developBranchName" pom.xml | grep -v "{" | cut -d'>' -f2 | cut -d'<' -f1)
if [ "$DEVELOP" == "" ]; then
  DEVELOP="develop"
fi

git checkout ${MASTER}

VERSION=$(grep -e '<version>' pom.xml | head -n 2 | tail -n 1 | cut -d'>' -f2 | cut -d'<' -f1)

git checkout ${DEVELOP}

LOG_DIR=/tmp/${PROJECT}-${VERSION}
mkdir -p ${LOG_DIR}

echo "projectId: $PROJECT"
echo "version:   $VERSION"
echo "log dir:   $LOG_DIR"

echo "Get staging id..."
STAGE_ID=$(mvn nexus-staging:rc-list -N | grep ${STAGE} | grep CLOSED | cut -d' ' -f2)

echo "Update changelog : $VERSION ($LOG_DIR/update-staging-changelog.log)"
rm -rf target/gitlab-cache
mvn -N -Pupdate-staging-changelog -Dgitlab.quiet=false -Dgitlab.stagingUrl=https://oss.sonatype.org/content/repositories/${STAGE}-${STAGE_ID} -Dgitlab.milestone=${VERSION} --log-file ${LOG_DIR}/update-staging-changelog.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_DIR}/update-staging-changelog.log
   exit 1
fi
git commit -m "Update changelog after stage of milestone $VERSION" CHANGELOG.md
git push