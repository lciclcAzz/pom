#!/usr/bin/env bash

# to execute a maven command
COMMAND_ARGS="$1"

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}
mkdir -p ${LOG_DIR}

LOG_FILE=${LOG_DIR}/maven-${COMMAND_ARGS// /}.log
echo "project:  $PROJECT"
echo "version:  $VERSION"
echo "log file: $LOG_FILE"

echo "Execute maven $COMMAND_ARGS"

mvn ${COMMAND_ARGS}  --log-file ${LOG_FILE}
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_FILE}
   exit 1
fi