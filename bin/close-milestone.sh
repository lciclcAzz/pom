#!/usr/bin/env bash

# to close a milestone

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)

DEVELOP=$(grep -e ".developBranchName" pom.xml | grep -v "{" | cut -d'>' -f2 | cut -d'<' -f1)
if [ "$DEVELOP" == "" ]; then
  DEVELOP="develop"
fi

git checkout ${DEVELOP}

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}
mkdir -p ${LOG_DIR}

echo "projectId: $PROJECT"
echo "version:   $VERSION"
echo "log dir:   $LOG_DIR"

echo "Closing gitlab milestone: $VERSION ($LOG_DIR/close-milestone.log)"
mvn gitlab:close-milestone -N --log-file ${LOG_DIR}/close-milestone.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_DIR}/close-milestone.log
   exit 1
fi
