#!/usr/bin/env bash

# to create a stage

PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)
MASTER=$(grep -e ".masterBranchName" pom.xml | grep -v "{" | cut -d'>' -f2 | cut -d'<' -f1)
if [ "$MASTER" == "" ]; then
  MASTER="master"
fi
DEVELOP=$(grep -e ".developBranchName" pom.xml | grep -v "{" | cut -d'>' -f2 | cut -d'<' -f1)
if [ "$DEVELOP" == "" ]; then
  DEVELOP="develop"
fi

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}
mkdir -p ${LOG_DIR}

echo "projectId: $PROJECT"
echo "master:    $MASTER"
echo "develop:   $DEVELOP"
echo "version:   $VERSION"
echo "log dir:   $LOG_DIR"

git checkout -B ${MASTER}
git checkout -B ${DEVELOP}

echo "Start release: $VERSION ($LOG_DIR/release-start.log)"
mvn jgitflow:release-start -B -Prelease-profile --log-file ${LOG_DIR}/release-start.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_DIR}/release-start.log
   exit 1
fi
echo "Finish release: $VERSION ($LOG_DIR/release-finish.log)"
mvn jgitflow:release-finish -Prelease-profile --log-file ${LOG_DIR}/release-finish.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_DIR}/release-finish.log
   exit 1
fi
