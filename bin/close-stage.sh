#!/usr/bin/env bash

# to close a stage

STAGE=$1
PROJECT=$(grep -e "<projectId>" pom.xml | grep -v '${' | cut -d'>' -f2 | cut -d'<' -f1)

VERSION=$(grep -e "-SNAPSHOT" pom.xml | cut -d'>' -f2 | cut -d'<' -f1)
VERSION=${VERSION/-SNAPSHOT/}
LOG_DIR=/tmp/${PROJECT}-${VERSION}
mkdir -p ${LOG_DIR}

echo "projectId: $PROJECT"
echo "stage:     $STAGE"
echo "log dir:   $LOG_DIR"

echo "Get staging id..."
STAGE_ID=$(mvn nexus-staging:rc-list -N | grep ${STAGE} | grep OPEN | cut -d' ' -f2)

echo "Closing stage: $STAGE_ID ($LOG_DIR/stage-close.log)"
mvn nexus-staging:close -N -DstagingRepositoryId=${STAGE_ID} --log-file ${LOG_DIR}/stage-close.log
if [ ! "$?" == "0" ]; then
   echo "Error"
   cat ${LOG_DIR}/stage-close.log
   exit 1
fi