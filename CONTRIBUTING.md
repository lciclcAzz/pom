# Release

```
wget -q -O - https://gitlab.com/ultreiaio/pom/raw/master/bin/close-milestone.sh | bash
wget -q -O - https://gitlab.com/ultreiaio/pom/raw/master/bin/create-stage.sh | bash
wget -q -O - https://gitlab.com/ultreiaio/pom/raw/master/bin/close-and-release-stage.sh | sed 's/$1/ultreiaio/' | bash
wget -q -O - https://gitlab.com/ultreiaio/pom/raw/master/bin/update-changelog.sh | bash
wget -q -O - https://gitlab.com/ultreiaio/pom/raw/master/bin/create-milestone.sh | bash
```
